" terminal settings
set encoding=utf-8
if !has('gui_running')
  set t_Co=256
endif
set noeb vb t_vb=

" theming
set background=dark
set laststatus=2
set noshowmode
let g:lightline = {'colorscheme': 'gruvbox'}
call matchadd('ColorColumn', '\%81v', 100)

" syntax highlighting
syntax enable
let g:is_bash = 1
colorscheme gruvbox

" text editing
set tabstop=8
set softtabstop=2
set shiftwidth=2
set expandtab
set backspace=2
set modeline
set number

" fzf / rg
set rtp+=/opt/fzf
command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
set grepprg=rg\ --vimgrep

" gitgutter
set updatetime=100
set signcolumn=yes
