# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# function to show return code with pretty colors
return_code() {
  RC="$?"
  RED="\033[1;31m"
  GREEN="\e[32;1m"
  OFF="\033[m"
  if [ "${RC}" -eq 0 ]; then
    echo -e "${GREEN}${RC}${OFF}"
  else
    echo -e "${RED}${RC}${OFF}"
  fi
}

# environment variables
GIT_EDITOR=vim
VISUAL=vim
EDITOR=vim
HISTCONTROL=ignoredups:erasedups  # no duplicate entries
HISTSIZE=100000                   # many commands in ongoing session memory
HISTFILESIZE=100000               # many lines in .bash_history
PS1='\[\e[33m\]\w\[\e[0m\] [`return_code`]\n\$ '

export -f return_code
export GIT_EDITOR VISUAL EDITOR HISTCONTROL HISTSIZE HISTFILESIZE IMAGE PS1

# FZF
fzfkb="/etc/fzf/key-bindings.bash"
if [ -f "$fzfkb" ]; then
  source "$fzfkb"
fi
FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'
export FZF_DEFAULT_COMMAND

# disable dumb bell
set bell-style none

# set bash to vi mode
#set -o vi

# append to history instead of overwriting
shopt -s histappend

# man / tldr
export LESS=-RXFi
export LESS_TERMCAP_mb=$(printf "\e[1;31m")
export LESS_TERMCAP_md=$(printf "\e[1;31m")
export LESS_TERMCAP_me=$(printf "\e[0m")
export LESS_TERMCAP_se=$(printf "\e[0m")
export LESS_TERMCAP_so=$(printf "\e[1;44;33m")
export LESS_TERMCAP_ue=$(printf "\e[0m")
export LESS_TERMCAP_us=$(printf "\e[1;32m")
mantldr() { man "$@" || tldr "$@" ; }

# aliases
#alias ls='ls --color'
alias ls='exa --git'
alias less='less -R'
alias cat='bat'
alias ping='prettyping --nolegend'
alias man='mantldr'

