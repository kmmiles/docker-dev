# vim: set filetype=sh:

REPO_CACHE="dev"
REPO_REGISTRY="kmmiles/${REPO_CACHE}"

################################################################################
# do initialiazation. set options and paths depending on whether this profile 
# was sourced from a script or directly on the shell.
################################################################################

if [ ! -z "${BASH_SOURCE[1]:-}" ]; then
  # sourced from script
  set -o errexit
  set -o pipefail
  set -o nounset

  __dir="$(cd "$(dirname "${BASH_SOURCE[1]}")" && pwd)"
  __file="${__dir}/$(basename "${BASH_SOURCE[1]}")"
  __root="$(cd "$(dirname "${__dir}")" && pwd)"
else
  # sourced from shell
  __dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  __file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
  __root="${__dir}"
fi

if [[ "${DEBUG:-}" ]]; then
  echo "*** Debug mode enabled ***"
  set -o xtrace
else
  set +o xtrace
fi

################################################################################
# Populate array of images
################################################################################
IMAGES=()
for dir in $(find "${__root}/images" -mindepth 1 -maxdepth 1); do
  IMAGES+=("$(basename "$dir")")
done

################################################################################
# find image in image array function
################################################################################
has_image() {
  local image="${1:-}"

  if [ "$image" == "" ]; then
    return 1
  fi

  if [[ " ${IMAGES[@]} " =~ " ${image} " ]]; then
    return 0
  fi

  return 1
}

print_config_json() {
  local image

  image="${1:-}"

  cat << EOF
{
  "image": "$image",
  "repo": "$REPO_CACHE",
  "repo_registry": "$REPO_REGISTRY",
  "tag": "$REPO_CACHE:$image",
  "tag_registry": "$REPO_REGISTRY:$image"
}
EOF
}

################################################################################
# logging routines
################################################################################
enable_logfile() {
  local logfile

  logfile="${1:-}"
  if [ "${logfile}" == "" ]; then
    logfile="${LOG_FILE:-}"
  fi

  if [ "${logfile}" != "" ]; then
#    exec 3>&1 1>>${logfile} 2>&1
    exec > >(tee -a ${logfile} )
    exec 2> >(tee -a ${logfile} >&2)
  fi
}

NC=$(echo -en '\033[0m')
RED=$(echo -en '\033[00;31m')
GREEN=$(echo -en '\033[00;32m')
YELLOW=$(echo -en '\033[00;33m')
LGRAY=$(echo -en '\033[00;37m')

logmsg() {
  printf "${@}\n"
}

loginfo() {
  printf "${LGRAY}${@}${NC}\n"
}

logwarn() {
  printf "${YELLOW}${@}${NC}\n"
}

logerr() {
  printf "${RED}${@}${NC}\n"
}

logsucc() {
  printf "${GREEN}${@}${NC}\n"
}

